package io.auroro.atifsaddique.pliro.service.network.Volley;

import org.json.JSONObject;

/**
 * Created by atifsaddique on 2/15/16.
 */
public interface NetworkResponseCallBack {
    void onSuccess(JSONObject jsonObject);

    void onNetworkFailure(String msg);
}
