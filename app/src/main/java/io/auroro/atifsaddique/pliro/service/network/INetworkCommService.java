package io.auroro.atifsaddique.pliro.service.network;

import com.android.volley.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import io.auroro.atifsaddique.pliro.service.network.Volley.NetworkResponseCallBack;

/**
 * Created by atifsaddique on 2/15/16.
 */
public interface INetworkCommService {
    public void getSpecialties(NetworkResponseCallBack callBack);

    public void getCities(NetworkResponseCallBack callBack);
}
