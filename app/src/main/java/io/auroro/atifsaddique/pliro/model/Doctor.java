package io.auroro.atifsaddique.pliro.model;

/**
 * Created by atifsaddique on 2/16/16.
 */
public class Doctor {
    private int id;
    private String firstName;
    private String lastsName;
    private int reviewCount;
    private int fee;
    private String hospitalName;
    private String hospitalAddress;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastsName() {
        return lastsName;
    }

    public void setLastsName(String lastsName) {
        this.lastsName = lastsName;
    }

    public int getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getHospitalAddress() {
        return hospitalAddress;
    }

    public void setHospitalAddress(String hospitalAddress) {
        this.hospitalAddress = hospitalAddress;
    }
}
