package io.auroro.atifsaddique.pliro.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import io.auroro.atifsaddique.pliro.R;
import io.auroro.atifsaddique.pliro.model.Doctor;
import io.auroro.atifsaddique.pliro.ui.adapter.DoctorListAdapter;

/**
 * Created by atifsaddique on 2/16/16.
 */
public class DoctorListFragment extends Fragment {
    private static final String TAG = "DoctorListFragment";

    private RecyclerView mRecyclerView;
    private DoctorListAdapter mDoctorListAdapter;
    private List<Doctor> mDoctors;


    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static DoctorListFragment newInstance() {
        DoctorListFragment fragment = new DoctorListFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_doctor_list, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.doctor_list_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        populateDummyData();
        mDoctorListAdapter = new DoctorListAdapter(mDoctors);
        mRecyclerView.setAdapter(mDoctorListAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        return rootView;
    }

    private void populateDummyData() {
        mDoctors = new ArrayList<>();
        Doctor doctor = new Doctor();
        doctor.setFirstName("Ali");
        doctor.setLastsName("Raza");
        doctor.setReviewCount(4);
        doctor.setFee(1000);
        doctor.setHospitalName("Mahroof");
        doctor.setHospitalAddress("F10-Markaz");
        mDoctors.add(doctor);

        doctor = new Doctor();
        doctor.setFirstName("Haseeb");
        doctor.setLastsName("Mazhar");
        doctor.setReviewCount(1);
        doctor.setFee(500);
        doctor.setHospitalName("Shifa");
        doctor.setHospitalAddress("i8-Markaz");
        mDoctors.add(doctor);

        doctor = new Doctor();
        doctor.setFirstName("Atif");
        doctor.setLastsName("Saddique");
        doctor.setReviewCount(2);
        doctor.setFee(600);
        doctor.setHospitalName("Mahroof");
        doctor.setHospitalAddress("F11-Markaz");
        mDoctors.add(doctor);
    }
}
