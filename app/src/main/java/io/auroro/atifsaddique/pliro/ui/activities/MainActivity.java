package io.auroro.atifsaddique.pliro.ui.activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import io.auroro.atifsaddique.pliro.R;
import io.auroro.atifsaddique.pliro.service.network.INetworkCommService;
import io.auroro.atifsaddique.pliro.service.network.NetworkCommService;
import io.auroro.atifsaddique.pliro.service.network.Volley.NetworkResponseCallBack;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    private static final String TAG = "MainActivity";
    private LinearLayout mSpecialtyLayout;
    private LinearLayout mLocationLayout;
    private LinearLayout mDateLayout;
    private TextView mTextSpecialty;
    private TextView mTextLocation;
    private TextView mTextDate;
    private Button mButtonSearch;
    private ArrayList<String> mSpecialties = new ArrayList<String>();
    private ArrayList<String> mCities = new ArrayList<String>();


    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormat;

    private INetworkCommService getNetworkService() {
        return NetworkCommService.getInstance(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        loadSpecialties();
        loadCities();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadCities() {
        getNetworkService().getCities(new NetworkResponseCallBack() {
            @Override
            public void onSuccess(JSONObject jsonObject) {
                Log.d(TAG, "onSuccess() called with: " + "jsonObject = [" + jsonObject.toString() + "]");
                mCities = parseObject(jsonObject);
            }

            @Override
            public void onNetworkFailure(String msg) {

            }
        });
    }

    private void loadSpecialties() {
        getNetworkService().getSpecialties(new NetworkResponseCallBack() {
            @Override
            public void onSuccess(JSONObject jsonObject) {
                Log.d(TAG, "onSuccess() called with: " + "jsonObject = [" + jsonObject.toString() + "]");
                mSpecialties = parseObject(jsonObject);
            }

            @Override
            public void onNetworkFailure(String msg) {
                Log.d(TAG, "onNetworkFailure() called with: " + "msg = [" + msg + "]");
            }
        });
    }

    private ArrayList<String> parseObject(JSONObject response) {
        JSONArray data = null;
        ArrayList<String> objects = new ArrayList<String>();
        try {
            data = response.getJSONArray("response");
            for (int i = 0; i < data.length(); i++) {
                try {
                    JSONObject object = data.getJSONObject(i);
                    objects.add(object.getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return objects;
    }

    private void initViews() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mSpecialtyLayout = (LinearLayout) findViewById(R.id.specialty_layout);
        mSpecialtyLayout.setOnClickListener(this);
        mLocationLayout = (LinearLayout) findViewById(R.id.location_layout);
        mLocationLayout.setOnClickListener(this);
        mDateLayout = (LinearLayout) findViewById(R.id.date_layout);
        mDateLayout.setOnClickListener(this);
        mTextSpecialty = (TextView) findViewById(R.id.specialty_text_view);
        mTextLocation = (TextView) findViewById(R.id.location_text_view);
        mTextDate = (TextView) findViewById(R.id.date_text_view);
        mButtonSearch = (Button) findViewById(R.id.search_button);
        mButtonSearch.setOnClickListener(this);
        setDateTimeField();
    }

    public void showSpecialtyPopUpListView(final int id, final String title, final ArrayList<String> items) {

        final String[] itemsArray = new String[items.size()];
        items.toArray(itemsArray);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(title);
        builder.setItems(itemsArray, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int index) {

                if (R.id.specialty_layout == id) {
                    mTextSpecialty.setText(itemsArray[index]);
                } else if (R.id.location_layout == id) {
                    mTextLocation.setText(itemsArray[index]);
                }
            }
        }).show();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (R.id.specialty_layout == id) {
            showSpecialtyPopUpListView(id, "Select specialty", mSpecialties);
        } else if (R.id.location_layout == id) {
            showSpecialtyPopUpListView(id, "Select your location", mCities);
        } else if (R.id.search_button == id) {
            gotoSearchActivity();
        } else if (R.id.date_layout == id) {
            fromDatePickerDialog.show();
        }


    }

    private void gotoSearchActivity() {
         Intent intent = new Intent(this, SearchActivity.class);
         startActivity(intent);
    }

    private void setDateTimeField() {
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateFormat = new SimpleDateFormat("EEEE dd-MM-yyyy");
                mTextDate.setText(dateFormat.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }
}
