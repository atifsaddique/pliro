package io.auroro.atifsaddique.pliro.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by atifsaddique on 2/16/16.
 */

import java.util.List;

import io.auroro.atifsaddique.pliro.R;
import io.auroro.atifsaddique.pliro.model.Doctor;

public class DoctorListAdapter extends RecyclerView.Adapter<DoctorListAdapter.ViewHolder> {
    private List<Doctor> mDoctors;

    public DoctorListAdapter(List<Doctor> mDoctors) {
        this.mDoctors = mDoctors;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_doctor, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        itemView.setTag(viewHolder);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Doctor doctor = getItem(position);
        holder.mTextReviewCount.setText(String.valueOf(doctor.getReviewCount()));
        holder.mTextFistName.setText(doctor.getFirstName());
        holder.mTextLastName.setText(doctor.getLastsName());
        holder.mTextFee.setText(String.valueOf(doctor.getFee()) + "$");
        holder.mTextHospitalName.setText(doctor.getHospitalName());
        holder.mTextHospitalAddress.setText(doctor.getHospitalAddress());


    }

    @Override
    public int getItemCount() {
        return mDoctors.size();
    }

    public Doctor getItem(int position) {
        return mDoctors.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageDoctorProfilePic;
        public TextView mTextReviewCount;
        public TextView mTextFistName;
        public TextView mTextLastName;
        public TextView mTextFee;
        public TextView mTextHospitalName;
        public TextView mTextHospitalAddress;

        public ViewHolder(View itemView) {
            super(itemView);
            mImageDoctorProfilePic = (ImageView) itemView.findViewById(R.id.doctor_profile_image);
            mTextReviewCount = (TextView) itemView.findViewById(R.id.review_count_text);
            mTextFistName = (TextView) itemView.findViewById(R.id.first_name_text);
            mTextLastName = (TextView) itemView.findViewById(R.id.last_name_text);
            mTextFee = (TextView) itemView.findViewById(R.id.fee_text);
            mTextHospitalName = (TextView) itemView.findViewById(R.id.hospital_name_text);
            mTextHospitalAddress = (TextView) itemView.findViewById(R.id.hospital_address_name_text);
        }
    }
}
