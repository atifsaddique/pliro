package io.auroro.atifsaddique.pliro.service.network;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.auroro.atifsaddique.pliro.BuildConfig;
import io.auroro.atifsaddique.pliro.service.network.Volley.NetworkResponseCallBack;
import io.auroro.atifsaddique.pliro.service.network.Volley.VolleySingleton;

/**
 * Created by atifsaddique on 2/15/16.
 */
public class NetworkCommService implements INetworkCommService {
    private static final String TAG = "NetworkCommService";

    private static final String WEB_SERVICE_PATH = BuildConfig.WEB_SERVER_NAME;
    private static final String CITIES_URL = WEB_SERVICE_PATH + "api/v1/cities";
    private static final String SPECIALTIES_URL = WEB_SERVICE_PATH + "api/v1/specialtys";
    private Context mContext;
    private static INetworkCommService mInstance;

    private NetworkCommService(Context context) {
        mContext = context;
    }

    public static INetworkCommService getInstance(Context context) {
        if (mInstance == null)
            mInstance = new NetworkCommService(context);
        return mInstance;
    }

    private RequestQueue getRequestQueue() {
        return VolleySingleton.getInstance(mContext).getRequestQueue();
    }

    private void addRequest(Request<JSONArray> request) {
        int requestTimeout = 30000;
        request.setRetryPolicy(new DefaultRetryPolicy(requestTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS));
        getRequestQueue().add(request);

    }

    @Override
    public void getSpecialties(final NetworkResponseCallBack callBack) {
        JsonArrayRequest request = new JsonArrayRequest(SPECIALTIES_URL,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("response",response);
                            callBack.onSuccess(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        addRequest(request);
    }

    @Override
    public void getCities(final NetworkResponseCallBack callBack) {
        JsonArrayRequest request = new JsonArrayRequest(CITIES_URL,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("response",response);
                            callBack.onSuccess(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        addRequest(request);
    }
}
